# 树莓派3B快速上手 OpenHarmony

## 预编译固件

| 镜像     | 下载地址                                                     |
| -------- | ------------------------------------------------------------ |
| 完整镜像 | [oh-rpi3b-full.img.xz](https://mirror.iscas.ac.cn/OpenHarmony/os/2.0/images/rpi-3b/20210823/oh-rpi3b-full.img.xz) |

## 烧录方式

下载完整镜像，准备一张至少1GB的SD卡

将SD卡通过读卡器连接电脑。

**Windows**

1. 使用解压工具解压oh-rpi3b-full.img.xz，得到oh-rpi3b-full.img。

2. 使用[Win32DiskImager](https://sourceforge.net/projects/win32diskimager/)进行烧录

**Ubuntu**

```bash
xzcat oh-rpi3b-full.img.xz | sudo dd of=/dev/your_sdcard bs=8M
```

## 分区方案

| 分区   | 大小 | 文件系统 |
| ------ | ---- | -------- |
| BOOT   | 64M  | vfat     |
| SYSTEM | 512M | ext4     |
| VENDOR | 64M  | ext4     |
| DATA   | 300M | ext4     |

## 编译方法

### 搭建编译环境

安装Ubuntu 18.04环境

```bash
# 安装必要的包
sudo apt update
sudo apt install -y binutils git git-lfs gnupg flex bison gperf build-essential \
                    zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 \
                    lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache \
                    libgl1-mesa-dev libxml2-utils xsltproc unzip m4 wget bc python python3 \
                    android-tools-fsutils libssl-dev mtools

# 安装repo
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 | sudo tee /usr/local/bin/repo >/dev/null
sudo chmod a+x /usr/local/bin/repo

# 将/bin/sh设置为/bin/bash
sudo ln -sf bash /bin/sh

# 设置git信息
git config --global user.name your_name
git config --global user.email your_email
```

### 下载编译内核需要的工具链
```
mkdir ~/ohos
cd ~/ohos
wget https://releases.linaro.org/components/toolchain/binaries/7.5-2019.12/arm-linux-gnueabihf/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz
tar -xvf gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz
```

### 编译OpenHarmony

#### 拉取源码

```bash
cd ~/ohos
mkdir openharmony
cd openharmony
repo init -u https://gitee.com/openharmony-sig/manifest.git -m devboard_rpi3b.xml -b master --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
./build/prebuilts_download.sh
```

#### 打补丁

```bash
cd ~/ohos/openharmony
python3 projectpatch/patch.py
```

#### 开始编译

```bash
cd ~/ohos
export PATH="$PATH:$(pwd)/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf/bin"
cd openharmony
python3 build-rpi3.py build
```

### 烧录到SD卡

固件位置生成位置 out/ohos-arm-release/packages/phone/images/firmware.img

#### 使用Win32DiskImager烧录

选择firmware.img进行烧录即可

#### 使用build-rpi3.py烧录

build-rpi3.py 脚本可以更安全的烧录镜像，并提供烧录进度显示

```bash
# 假设SD卡的设备文件位于/dev/sdx
sudo python3 build-rpi3.py flash /dev/sdx
```

#### 使用dd烧录

dd命令是常用的烧录命令，使用dd命令时没有额外的安全检查。

请仔细检查SD卡设备文件路径，否则容易带来灾难性后果。

```bash
# 假设SD卡的设备文件位于/dev/sdx
sudo dd if=out/ohos-arm-release/packages/phone/images/firmware.img of=/dev/sdx bs=8M
```
